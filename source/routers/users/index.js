import express from 'express'
import {
    get,
    post
} from './handlers'
import {
    getByHash,
    putByHash,
    deleteByHash
} from './hash'
import {
    validator
} from '../../utils'

import {
    createUser
} from '../../schemas'

const router = express.Router()




router.get('/', get)
router.post('/', [validator(createUser)], post)
router.get('/:userHash', getByHash)
router.put('/:userHash', [validator(createUser)], putByHash)
router.delete('/:userHash', deleteByHash)

export {
    router as users
}