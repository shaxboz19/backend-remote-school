import {
    Users
} from '../../../controllers'
export const getByHash = async (req, res) => {
    try {
        const users = new Users()
        const data = await users.getByHash(req.params.userHash)
        res.status(200).json(data)
    } catch (e) {
        res.status(400).json({
            message: e.message
        })
    }
}

export const putByHash = async (req, res) => {
    try {
        const {
            userHash
        } = req.params
        const users = new Users(req.body)
        const data = await users.update({
            hash: userHash
        })
        res.status(200).json({
            data
        })
    } catch (e) {
        res.status(400).json({
            message: e.message
        })
    }
}

export const deleteByHash = async (req, res) => {
    try {
        const user = new Users()
        await user.deleteByHash(req.params.userHash)
        res.sendStatus(204)
    } catch (e) {
        res.status(400).json({
            message: e.message
        })
    }
}