import {
    Users
} from '../../controllers'


export const get = async (req, res) => {
    try {
        const users = new Users()
        const data = await users.getAll()
        res.status(200).json(data)

    } catch (e) {
        res.status(400).json({
            message: e.message
        })
    }
}

export const post = async (req, res) => {
    try {
        const user = new Users(req.body)

        const {
            hash
        } = await user.create()
        res.status(201).json({
            hash
        })

    } catch (e) {
        res.status(400).json({
            message: e.message
        })
    }
}