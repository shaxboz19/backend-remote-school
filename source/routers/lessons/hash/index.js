export const getByHash = (req, res) => {
    try {
        res.status(200).json({lesson : {
            id: 3, title : 'test4'
            }})
    } catch (e) {
        res.status(400).json({message : e.message})
    }
}

export const putByHash = (req, res) => {
    try {
        res.status(200).json({lesson : {
                id: 3, title : 'test5'
            }})
    } catch (e) {
        res.status(400).json({message : e.message})
    }
}
export const deleteByHash = (req, res) => {
    try {
        res.status(200).json({lesson : {
                id: 3, title : 'test5'
            }})
    } catch (e) {
        res.status(400).json({message : e.message})
    }
}


export const addVideo = (req, res) => {
    try  {
        res.status(201).json({id : req.body})
    } catch (e) {
        res.status(400).json({message : e.message})
    }
}

export const addKeyNotes = (req, res) => {
    try  {
        res.status(201).json({id : req.body})
    } catch (e) {
        res.status(400).json({message : e.message})
    }
}

export const getVideoByHash = (req, res) => {
    try  {
        res.status(200).json({id : req.body})
    } catch (e) {
        res.status(400).json({message : e.message})
    }
}

export const deleteVideoByHash = (req, res) => {
    try  {
        res.status(204).json({id : req.body})
    } catch (e) {
        res.status(400).json({message : e.message})
    }
}

export const getKeynotesByHash = (req, res) => {
    try {
        res.status(200).json({id : req.params.keynotesHash})
    }catch (e) {
        res.status(400).json({message : e.message})
    }
}

export const deleteKeynotesByHash = (req, res) => {
    try {
        res.status(204).json({id : req.headers.keynotesHash})
    }catch (e) {
        res.status(400).json({message : e.message})
    }
}