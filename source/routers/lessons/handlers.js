import {
    Lessons
} from '../../controllers'
export const get = (req, res) => {
    try {
        res.status(200).json({
            lesson: {
                id: 1,
                title: 'test'
            }
        })
    } catch (e) {
        res.status(400).json({
            message: e.message
        })
    }
}
export const post = async (req, res) => {
    try {
        const lesson = new Lessons(req.body)
        const data = await lesson.create()
        res.status(201).json(data)
    } catch (e) {
        res.status(400).json({
            message: e.message
        })
    }
}