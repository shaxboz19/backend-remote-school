import express from "express";

import {get, post} from './handlers'

import {validator} from '../../utils/validator'
import {createLessons, addVideo as addVideoSchema, addKeynotes as addKeynotesSchema } from '../../schemas'

import {getByHash, putByHash, deleteByHash, addVideo, addKeyNotes, getVideoByHash, deleteVideoByHash, getKeynotesByHash, deleteKeynotesByHash} from './hash'

const router = express.Router()


router.get('/', get)
router.post('/', [validator(createLessons)], post)

router.get('/:lessonHash', getByHash)
router.put('/:lessonHash', [validator(createLessons)], putByHash)
router.delete('/:lessonHash', deleteByHash)

router.post('/:lessonHash/videos', [validator(addVideoSchema)], addVideo)

router.post('/:lessonHash/keynotes', [validator(addKeynotesSchema)], addKeyNotes)

router.post('/:lessonHash/videos/:videoHash', getVideoByHash)
router.delete('/:lessonHash/videos/:videoHash', deleteVideoByHash)

router.get('/:lessonHash/keynotes/:keynotesHash', getKeynotesByHash)
router.delete('/:lessonHash/keynotes/:keynotesHash', deleteKeynotesByHash)

export {router as lessons}