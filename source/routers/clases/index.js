import express from "express";

import {
    get,
    post
} from './handlers'

import {
    validator
} from '../../utils/validator'
import {
    createClasses,
    enrollSchema,
    expelSchema
} from '../../schemas'

import {
    getByHash,
    putByHash,
    deleteByHash,
    addStudent,
    removeStudent
} from "./hash";

const router = express.Router()

router.get('/', get)
router.post('/', [validator(createClasses)], post)
router.get('/:classHash', getByHash)
router.put('/:classHash', [validator(createClasses)], putByHash)
router.delete('/:classHash', deleteByHash)

// EDUCATION

router.post('/:classHash/enroll', [validator(enrollSchema)], addStudent)
router.post('/:classHash/expel', [validator(expelSchema)], removeStudent)


export {
    router as clases
}