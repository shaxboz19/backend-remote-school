import {
    Classes
} from '../../../controllers'
export const getByHash = async (req, res) => {
    try {
        const classes = new Classes()
        const data = await classes.getByHash(req.params.classHash)
        res.status(200).json(data)
    } catch (e) {
        res.status(400).json({
            message: e.message
        })
    }
}

export const putByHash = async (req, res) => {
    try {
        const classes = new Classes(req.body)
        const data = await classes.update(req.params.classHash)
        res.status(200).json(data)
    } catch (e) {
        res.status(400).json({
            message: e.message
        })
    }
}
export const deleteByHash = async (req, res) => {
    try {
        const classes = new Classes()
        await classes.deleteByHash(req.params.classHash)
        res.sendStatus(204)
    } catch (e) {
        res.status(400).json({
            message: e.message
        })
    }
}

export const addStudent = async (req, res) => {
    try {
        const classes = new Classes(req.body)
        const data = await classes.addStudent(req.params.classHash)
        res.status(201).json(data)
    } catch (e) {
        res.status(400).json({
            message: e.message
        })
    }
}

export const removeStudent = async (req, res) => {
    try {
        const classes = new Classes(req.body)
        const data = await classes.removeStudent(req.params.classHash)
        res.status(201).json(data)
    } catch (e) {
        res.status(400).json({
            message: e.message
        })
    }
}