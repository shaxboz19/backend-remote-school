import {
    Classes
} from '../../controllers'
export const get = async (req, res) => {
    try {
        const classes = new Classes()
        const data = await classes.getAll()
        res.status(200).json(data)
    } catch (e) {
        res.status(400).json({
            message: e.message
        })
    }
}

export const post = async (req, res) => {
    try {
        const classes = new Classes(req.body)
        const data = await classes.create()
        res.status(201).json({
            data
        })
    } catch (e) {
        res.status(400).json({
            message: e.message
        })
    }
}