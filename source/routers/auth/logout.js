import express from 'express';
import {logout} from './handlers'


const router = express.Router();

router.post('/', logout)

export {router as logout}