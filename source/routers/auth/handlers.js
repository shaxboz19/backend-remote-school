import {
    users
} from '../../odm'
import {
    tokens
} from '../../database/tokens.json';

const fs = require('fs');
const {
    promisify
} = require('util');
const readFile = promisify(fs.readFile);
const writeFile = promisify(fs.writeFile);
const path = require('path');
import jwt from 'jsonwebtoken';


export const login = async (req, res) => {
    try {


        const {
            email,
            password
        } = req.body
        const user = await users.findOne({
            email
        })
        const isMatch = await user.comparePassword(password)

        if (!isMatch) {
            res.status(400).json({
                message: 'User not found'
            })
        } else {
            const token = jwt.sign({
                email: user.email
            }, process.env.SECRET_KEY, {
                expiresIn: '1h'
            })
            readFile(path.join('./source/database', 'tokens.json'), 'utf8').then(data => {
                const tokens = JSON.parse(data).tokens
                tokens[user.email] = token
                writeFile(path.join('./source/database', 'tokens.json'), JSON.stringify({
                    tokens
                }), 'utf8').then(() => {
                    res.setHeader('x-token', token)
                    return res.status(200).json({
                        message: 'User found'
                    })
                })
            }).catch(err => {
                console.log(err)
            })
        }
    } catch (e) {
        res.status(400).json({
            message: e.message
        })
    }

}

export const logout = async (req, res) => {
    const [type, token] = req.headers['authorization'].split(' ')
    try {
        const user = jwt.verify(token, process.env.SECRET_KEY)
        if (tokens[user.email]) {
            delete tokens[user.email]
            await writeFile(path.join('./source/database', 'tokens.json'), JSON.stringify({
                tokens
            }), 'utf8')
            return res.status(200).json({
                message: 'Logout success'
            })
        }
        return res.status(400).json({
            message: 'Logout failed'
        })

    } catch (err) {
        return res.status(400).json({
            message: 'Invalid token'
        })
    }


}