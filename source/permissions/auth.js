import {
    ExtractJwt,
    Strategy as JwtStrategy
} from "passport-jwt";
import passport from "passport";
import {
    tokens
} from "../database/tokens.json";


const options = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: process.env.SECRET_KEY
}

passport.use(new JwtStrategy(options, (jwt_payload, done) => {
    const user = tokens[jwt_payload.email];
    if (user) {
        return done(null, user);
    }
    return done(null, false);
}));

const authPermission = passport.authenticate('jwt', {
    session: false
})


export {
    authPermission
};