// Core
import express from 'express';
import session from 'express-session';
import bodyParser from 'body-parser';

// Instruments
import {
    sessionOptions,
} from './utils';

// Routers

const app = express();

app.use(bodyParser.json({
    limit: '10kb'
}));

app.use(session(sessionOptions));
app.use(express.json({
    limit: '10kb'
}));

export {
    app
};