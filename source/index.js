// Core
import debug from 'debug';

// Instruments
import {
    app
} from './server';
import {
    getPort
} from './utils';
import {authPermission} from './permissions'


import * as routers from './routers'




// DB
import './db';

const PORT = getPort();
const dg = debug('server:main');

app.use('/users', [authPermission],   routers.users);
app.use('/classes', [authPermission],  routers.clases);
app.use('/lessons', [authPermission],  routers.lessons);
app.use('/login',  routers.login);
app.use('/logout',  routers.logout);

app.listen(PORT, () => {
    dg(`Server API is up on port ${PORT}`);
});