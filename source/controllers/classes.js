import {
    ClassesModel
} from '../models'

export class Classes {
    constructor(data) {
        this.models = {
            classes: new ClassesModel(data)
        }
    }
    async create() {
        try {
            const data = await this.models.classes.create()
            return data
        } catch ({
            message
        }) {
            throw new Error(message)
        }
    }
    async update(hash) {
        try {
            const data = await this.models.classes.update(hash)
            return data
        } catch ({
            message
        }) {
            throw new Error(message)
        }
    }
    async deleteByHash(hash) {
        try {
            const data = await this.models.classes.deleteByHash(hash)
            return data
        } catch ({
            message
        }) {
            throw new Error(message)
        }
    }
    async getAll() {
        try {
            const data = await this.models.classes.getAll()
            return data
        } catch ({
            message
        }) {
            throw new Error(message)
        }
    }
    async getByHash(hash) {
        try {
            const data = await this.models.classes.getByHash(hash)
            return data
        } catch ({
            message
        }) {
            throw new Error(message)
        }
    }
    async addStudent(hash) {
        try {

            const data = await this.models.classes.addStudent(hash)
            return data
        } catch ({
            message
        }) {
            throw new Error(message)
        }
    }
    async removeStudent(hash) {
        try {
            const data = await this.models.classes.removeStudent(hash)
            return data
        } catch ({
            message
        }) {
            throw new Error(message)
        }

    }

}