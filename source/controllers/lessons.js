import {
    LessonsModel
} from '../models'

export class Lessons {
    constructor(data) {
        this.models = {
            lessons: new LessonsModel(data)
        }
    }
    create() {
        try {
            const data = this.models.lessons.create()
            return data
        } catch ({
            message
        }) {
            throw new Error(message)
            console.error(message);
        }
    }
}