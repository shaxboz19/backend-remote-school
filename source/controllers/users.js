import {
    UsersModel
} from '../models'

class Users {
    constructor(data) {
        this.models = {
            users: new UsersModel(data)
        }

    }
    async create() {
        try {
            const data = await this.models.users.create()
            return data
        } catch ({
            message
        }) {
            throw new Error(message)
        }
    }
    async update({
        hash,
    }) {
        try {
            const answer = await this.models.users.update({
                hash,
            })
            return answer
        } catch ({
            message
        }) {
            throw new Error(message)
        }
    }
    async getAll() {
        try {
            const data = await this.models.users.getAll()
            return data

        } catch ({
            message
        }) {
            throw new Error(message)
        }
    }
    async getByHash(hash) {
        try {
            const data = await this.models.users.getByHash(hash)
            return data
        } catch ({
            message
        }) {
            throw new Error(message)
        }
    }
    async deleteByHash(hash) {
        try {
            const data = await this.models.users.deleteByHash(hash)
            return data
        } catch ({
            message
        }) {
            throw new Error(message)
        }
    }
}

export {
    Users
}