import {
    lessons
} from '../odm'

export class LessonsModel {
    constructor(data) {
        this.data = data
    }
    async create() {
        try {
            const data = await lessons.create(this.data)
            return data
        } catch ({
            message
        }) {
            throw new Error(message)
            console.error(message);
        }
    }
}