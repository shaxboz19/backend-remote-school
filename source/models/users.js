import {
    users
} from '../odm'

export class UsersModel {
    constructor(data) {

        this.data = data
    }
    async create() {
        try {
            const data = await users.create(this.data)
            return data
        } catch ({
            message
        }) {
            throw new Error(message)
        }
    }
    async update({
        hash,
    }) {
        try {
            const answer = await users.findOneAndUpdate({
                hash
            }, this.data, {
                new: true
            })
            return answer
        } catch ({
            message
        }) {
            throw new Error(message)
        }
    }
    async getAll() {
        try {
            const data = await users.find({}).lean()
            return data

        } catch ({
            message
        }) {
            throw new Error(message)
        }
    }
    async getByHash(hash) {
        try {
            const data = await users.findOne({
                hash
            }).select({
                __v: false
            }).lean()
            return data

        } catch ({
            message
        }) {
            throw new Error(message)
        }
    }
    async deleteByHash(hash) {
        try {
            const data = await users.findOneAndDelete({
                hash
            })
            return data

        } catch ({
            message
        }) {
            throw new Error(message)
        }
    }
}