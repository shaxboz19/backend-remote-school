import {
    classes,
    users
} from '../odm'

export class ClassesModel {
    constructor(data) {
        this.data = data
    }
    async create() {
        try {
            const data = await classes.create(this.data)
            return data
        } catch ({
            message
        }) {
            throw new Error(message)
        }
    }
    async update(hash) {
        try {
            const data = await classes.findOneAndUpdate({
                hash
            }, this.data, {
                new: true
            })
            return data
        } catch ({
            message
        }) {
            throw new Error(message)
        }
    }
    async deleteByHash(hash) {
        try {
            const data = await classes.findOneAndDelete({
                hash
            })
            return data
        } catch ({
            message
        }) {
            throw new Error(message)
        }
    }
    async getAll() {
        try {
            const data = await classes.find({})
            return data
        } catch ({
            message
        }) {
            throw new Error(message)
        }
    }
    async getByHash(hash) {
        try {
            const data = await classes.findOne({
                hash
            })
            return data
        } catch ({
            message
        }) {
            throw new Error(message)
        }
    }
    async addStudent(hash) {
        try {
            const {
                _id
            } = await users.findOne({
                hash: this.data.user
            })
            const data = await classes.findOneAndUpdate({
                hash
            }, {
                $push: {
                    students: {
                        user: {
                            _id
                        },
                        notes: this.data.note,
                        status: this.data.status

                    }
                }

            }, {
                new: true,
            })
            return data
        } catch ({
            message
        }) {
            throw new Error(message)
        }
    }
    async removeStudent(hash) {
        try {
            const {
                _id
            } = await users.findOne({
                hash: this.data.user
            })
            const data = await classes.findOneAndUpdate({
                hash
            }, {
                $pull: {
                    students: {
                        user: {
                            _id
                        }
                    }
                }

            }, {
                new: true,
            })
            return data
        } catch ({
            message
        }) {
            throw new Error(message)
        }
    }

}