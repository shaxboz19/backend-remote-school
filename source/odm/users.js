import mongoose from "mongoose";
import validator from "validator";
import bcrypt from 'bcrypt'
import {
    v4 as uuidv4
} from 'uuid';
const SALT_WORK_FACTOR = 10;
const userSchema = new mongoose.Schema({
    name: {
        first: {
            type: String,
            minlength: 3,
            index: true

        },
        last: {
            type: String,
            minlength: 3,
            index: true
        }
    },
    email: {
        type: String,
        required: 'Email is required',
        unique: true,
        trim: true,
        lowercase: true,
        validate: [validator.isEmail, 'Please fill a valid email address'],

    },
    phone: {
        type: String,
        minlength: 12,
        trim: true,
        index: 'text'
    },
    password: {
        type: String,
        minlength: 6
    },
    sex: {
        type: String,
        enum: ['M', 'F', 'm', 'f']
    },
    role: {
        type: String,
        enum: ['admin', 'student', 'teacher']
    },
    hash: {
        type: String,
        default: () => uuidv4(),
    }

}, {
    timestamps: true
})
userSchema.pre('save', function (next) {
    if (!this.isModified('password')) {
        return next();
    }
    bcrypt.genSalt(SALT_WORK_FACTOR, (err, salt) => {
        if (err) {
            return next(err);
        }
        bcrypt.hash(this.password, salt, (err, hash) => {
            if (err) {
                return next(err);
            }
            this.password = hash;
            next();
        })
    })
})

userSchema.virtual('fullName').get(function () {
    return `${this.name.first} ${this.name.last}`
})

userSchema.methods.comparePassword = function (candidatePassword) {
    return new Promise((resolve, reject) => {
        bcrypt.compare(candidatePassword, this.password, (err, isMatch) => {
            if (err) {
                reject(err);
            }
            resolve(isMatch);
        })
    })
}

const users = mongoose.model('user', userSchema);
export {
    users
};