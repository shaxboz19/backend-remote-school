import mongoose from "mongoose";
import {
    v4 as uuidv4
} from 'uuid';

const lessonsSchema = new mongoose.Schema({
    title: {
        type: String,
    },
    description: {
        type: String,
    },
    order: {
        type: Number,
    },
    hash: {
        type: String,
        set(hash) {
            return new Error('hash is not writable')
        },
        default: () => uuidv4()
    },
    availability: {
        type: String,
    },
    content: {
        videos: [{
            hash: {
                type: String,
                unique: true
            },
            title: {
                type: String,
            },
            order: {
                type: Number,
                index: 1
            },
            url: {
                type: String,
            }
        }],
        keynotes: [{
            hash: {
                type: String,
                unique: true
            },
            title: {
                type: String,
            },
            order: {
                type: Number,
            },
            uri: {
                type: String,
            }
        }]
    }
}, {
    timestamps: true
})

const lessons = new mongoose.model('lessons', lessonsSchema)
export {
    lessons
}