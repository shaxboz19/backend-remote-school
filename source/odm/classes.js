import mongoose from "mongoose";
import {
    v4 as uuidv4
} from 'uuid';
const userSchema = new mongoose.Schema({

    user: {
        _id: {
            type: mongoose.SchemaTypes.ObjectId,
            ref: 'users',


        },
    },
    status: {
        type: String,
        enum: ['active', 'inactive'],
    },
    notes: {
        type: String,
    }
})
const classesSchema = new mongoose.Schema({
    title: {
        type: String,
        index: "text"
    },
    description: {
        type: String,
        index: "text"
    },
    order: {
        type: Number,
        index: 1
    },
    hash: {
        type: String,
        default: () => uuidv4()
    },
    duration: {
        started: {
            type: Date,
        },
        closed: {
            type: Date,
        }
    },
    students: [userSchema],
    lessons: [{
        lesson: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "lessons"
        },
        scheduled: {
            type: Date,
        }
    }]
})

const classes = new mongoose.model('classes', classesSchema)
export {
    classes
}