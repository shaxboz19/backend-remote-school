export const createUser = {
    type: 'object',
    properties: {
        name: {
            type: "object",
            properties: {
                first: {
                    type: "string",
                    minLength: 3,
                },
                last: {
                    type: "string",
                    minLength: 3,
                }
            },
            required: ["first", "last"]
        },
        email: {
            type: "string",
            format: "email",
        },
        phone: {
            type: "string",
            minLength: 12
        },
        password: {
            type: 'string',
            minLength: 6
        },
        sex: {
            type: 'string',
            enum: ['M', 'F', 'm', 'f']
        },
        role: {
            type: "string",
            enum: ['admin', 'student', 'teacher']
        }

    },
    required: ['name', 'email', 'password', 'role']
}