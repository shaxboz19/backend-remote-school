export {
    createUser
}
from './user.schema'

export { createClasses } from './classes.schema'
export { createLessons } from './lessons.schema'
export { enrollSchema } from './enroll.schema'
export { expelSchema } from './expel.schema'
export { addVideo } from './modules/addVideo.schema'
export { addKeynotes } from './modules/addKeynotes.schema'