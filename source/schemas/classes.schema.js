export const createClasses = {
    type: 'object',
    properties: {
        title: {
            type: 'string'
        },
        description: {
            type: 'string'
        },
        order: {
            type: 'number'
        },
        duration: {
            type: 'object',
            required: ['started', 'closed'],
            properties: {
                started: {
                    type: 'string',
                    format: 'date-time'
                },
                closed: {
                    type: 'string',
                    format: 'date-time'
                }
            }
        },
        students: {
            type: 'array',
            items: {
                type: 'object',
                properties: {
                    user: {
                        type: 'string'

                    },
                    status: {
                        type: 'string'
                    },
                    expelled: {
                        type: 'boolean'
                    },
                    notes: {
                        type: 'string'
                    }
                },
                required: ['user', 'status', 'expelled']
            }
        },
        lessons: {
            type: 'array',
            items: {
                type: 'object',
                properties: {
                    lesson: {
                        type: 'string'
                    },
                    scheduled: {
                        type: 'string',
                        format: 'date-time'
                    }
                }
            }
        }
    },
    required: ['title', 'description']
}