import {
    addVideo
} from "./modules/addVideo.schema";
import {
    addKeynotes
} from "./modules/addKeynotes.schema";

export const createLessons = {
    type: 'object',
    required: ['title'],
    properties: {
        title: {
            type: 'string'
        },
        description: {
            type: 'string'
        },
        hash: {
            type: 'string'
        },
        order: {
            type: 'number'
        },
        availability: {
            type: 'string'
        },
        content: {
            type: 'object',
            required: ['videos'],
            properties: {
                videos: {
                    type: 'array',
                    minItems: 1,
                    items: [
                        addVideo
                    ]
                },
                keynotes: {
                    type: 'array',
                    minItems: 1,
                    items: [
                        addKeynotes
                    ]
                }
            }
        },
        createdAt: {
            type: 'string',
            format: 'date-time'
        },
        modifiedAt: {
            type: 'string',
            format: 'date-time'
        }
    }
}