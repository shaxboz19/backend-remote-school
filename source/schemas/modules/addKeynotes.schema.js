export const addKeynotes = {
    type: 'object', required: ['title', 'url'], properties: {
        title: {
            type: 'string'
        },
        order: {
            type: 'number'
        },
        url: {
            type: 'string',
            format: 'uri'
        },
    }
}