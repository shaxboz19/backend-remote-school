export const enrollSchema = {
  type: 'object',
  properties: {
    user: {
      type: 'string',
      minLength: 1,
    },
    status: {
      type: 'string',
      minLength: 1,
    },
    notes: {
      type: 'string',
      minLength: 1,
    },
  },
  required: ['user', 'status'],
  additionalProperties: false,
};